# Palette

Print color values (Hex RGB, RGB, Xterm) for an itermcolors file.

## Installation

``go get gitlab.com/hackebrot/palette``

## Usage

Download an ``*.itermcolors`` file or export it from [iTerm2][iTerm2].

To print color information for a color palette file run:

    $ palette -colors <itermcolors-file>

If no errors occur, you will see several color values for your palette:

```no-highlight
Ansi 0 Color            Hex RGB #4e4e4e     RGB  78  78  78     Xterm code 239
Ansi 1 Color            Hex RGB #d68787     RGB 214 135 135     Xterm code 174
Ansi 2 Color            Hex RGB #5f865f     RGB  95 134  95     Xterm code  65
Ansi 3 Color            Hex RGB #d8af5f     RGB 216 175  95     Xterm code 179
Ansi 4 Color            Hex RGB #85add4     RGB 133 173 212     Xterm code 110
Ansi 5 Color            Hex RGB #d7afaf     RGB 215 175 175     Xterm code 181
Ansi 6 Color            Hex RGB #87afaf     RGB 135 175 175     Xterm code 109
Ansi 7 Color            Hex RGB #d0d0d0     RGB 208 208 208     Xterm code 252
Ansi 8 Color            Hex RGB #626262     RGB  98  98  98     Xterm code 241
Ansi 9 Color            Hex RGB #d75f87     RGB 215  95 135     Xterm code 168
Ansi 10 Color           Hex RGB #87af87     RGB 135 175 135     Xterm code 108
Ansi 11 Color           Hex RGB #ffd787     RGB 255 215 135     Xterm code 222
Ansi 12 Color           Hex RGB #add4fb     RGB 173 212 251     Xterm code 153
Ansi 13 Color           Hex RGB #ffafaf     RGB 255 175 175     Xterm code 217
Ansi 14 Color           Hex RGB #87d7d7     RGB 135 215 215     Xterm code 116
Ansi 15 Color           Hex RGB #e4e4e4     RGB 228 228 228     Xterm code 254
Background Color        Hex RGB #3a3a3a     RGB  58  58  58     Xterm code 237
Badge Color             Hex RGB #ff0000     RGB 255   0   0     Xterm code   9
Bold Color              Hex RGB #e4e4e4     RGB 228 228 228     Xterm code 254
Cursor Color            Hex RGB #fed887     RGB 254 216 135     Xterm code 222
Cursor Guide Color      Hex RGB #535353     RGB  83  83  83     Xterm code 240
Cursor Text Color       Hex RGB #3a3a3a     RGB  58  58  58     Xterm code 237
Foreground Color        Hex RGB #d0d0d0     RGB 208 208 208     Xterm code 252
Link Color              Hex RGB #85add4     RGB 133 173 212     Xterm code 110
Selected Text Color     Hex RGB #d0d0d0     RGB 208 208 208     Xterm code 252
Selection Color         Hex RGB #4e657b     RGB  78 101 123     Xterm code  60
Tab Color               Hex RGB #6c6c6c     RGB 108 108 108     Xterm code 242
```

## What do I need ``Xterm codes`` for?!

I personally use these values for my bash prompt in iTerm2. Which looks as follows:

    green=$(tput setaf 108);

You can find my complete ``.bash_prompt`` at [github.com/hackebrot/dotfiles][dotfiles].

[dotfiles]: https://github.com/hackebrot/dotfiles
[iTerm2]: https://www.iterm2.com/
